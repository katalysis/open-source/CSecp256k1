// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "CSecp256k1",
    pkgConfig: "libsecp256k1",
    providers: [
        .brew(["secp256k1"]),
        .apt(["secp256k1"]),
    ]
)
